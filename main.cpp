#include <iostream>

using namespace std;

template <typename ... Args> int ArgsCount(Args ... args) {
    return sizeof...(args);
}

int main() {
    cout << ArgsCount(1, "world", "hello") << endl;
    cout << "Hello, World!" << endl;
    return 0;
}
